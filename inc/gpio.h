/*
 * gpio.h
 *
 *  Created on: 8 de abr de 2018
 *      Author: Mateus Lima
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "stm32f0xx_gpio.h"

void GPIO_Config(GPIO_TypeDef*, uint32_t GPIO_Pin, uint32_t Mode);

#endif /* GPIO_H_ */
