/**
  ******************************************************************************
  * @file    main.c
  * @author  Mateus Lima
  * @version V1.0
  * @date    04-April-2018
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f0xx.h"
#include "delay.h"
#include "oneWireSlave.h"

int main(){

	unsigned char id_Mateus[6] = {0x00, 0x00, 0x09, 0x52, 0x4C, 0x01};
	unsigned char id_Thiago[6] = {0x00, 0x00, 0x09, 0xE7, 0x56, 0x03};
	unsigned char *iButton_code = RFID_to_iButton(id_Mateus);

	while(!OWS_wait_reset());
	OWS_present();

	if(OWS_read_byte() == 0x33)
		send_iButton_code(iButton_code);

	while(1);

}
