/*
 * oneWireSlave.c
 *
 *  Created on: 3 de ago de 2018
 *      Author: mateus-trix
 */

#include "gpio.h"
#include "oneWireSlave.h"
#include "delay.h"

/**********************************************************************
* Function:        unsigned char crc8(const uint8_t *addr, uint8_t len)
* PreCondition:    None
* Input:		   None
* Output:		   None
* Overview:		   Return the crc8 code.
***********************************************************************/
unsigned char crc8(const uint8_t *addr, uint8_t len)
{
	uint8_t crc = 0;

	while (len--) {

		uint8_t inbyte = *addr++;
		for (uint8_t i = 8; i; i--) {
			uint8_t mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if (mix) crc ^= 0x8C;
			inbyte >>= 1;
		}
	}
	return crc;
}

/**********************************************************************
* Function:        void OWS_drive_low()
* PreCondition:    None
* Input:		   None
* Output:		   None
* Overview:		   Configure the OW_PIN as Output and drive the OW_PIN LOW.
***********************************************************************/
void OWS_drive_low(){

	GPIO_ResetBits(oneWirePort, oneWirePin);

}

/**********************************************************************
* Function:        void OWS_drive_high()
* PreCondition:    None
* Input:		   None
* Output:		   None
* Overview:		   Configure the OW_PIN as Output and drive the OW_PIN HIGH.
***********************************************************************/
void OWS_drive_high(){

	GPIO_SetBits(oneWirePort, oneWirePin);

}

/**********************************************************************
* Function:        unsigned char OWS_drive_read()
* PreCondition:    None
* Input:		   None
* Output:		   Return the status of OW pin.
* Overview:		   Configure as Input pin and Read the status of OW_PIN
***********************************************************************/
unsigned char OWS_drive_read(){

	return GPIO_ReadInputDataBit(oneWirePort, oneWirePin);

}

/**********************************************************************
* Function:        void OWS_write_bit(unsigned char write_bit)
* PreCondition:    None
* Input:		   Write a bit to 1-wire slave device.
* Output:		   None
* Overview:		   This function used to transmit a single bit to slave device.
*
***********************************************************************/

void OWS_write_bit(unsigned char write_bit){

	if(write_bit == HIGH){
		//escreve 1
		while(OWS_drive_read());
		while(!OWS_drive_read());
	}
	else if(write_bit == LOW){
		//escreve 0
		while(OWS_drive_read());
		GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_OUT);
		OWS_drive_low();
		Delay_us(0);
		GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_IN);
	}
	else{
		//escreve 1 com um delay ajustado
		while(OWS_drive_read());
		GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_OUT);
		OWS_drive_low();
		GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_IN);
		Delay_us(10);
	}

}


/**********************************************************************
* Function:        unsigned char OW_read_bit (void)
* PreCondition:    None
* Input:		   None
* Output:		   Return the status of the OW PIN
* Overview:		   This function used to read a single bit from the slave device.
*
***********************************************************************/

unsigned char OWS_read_bit(){

	unsigned char read_data;
	//reading a bit
    while(!OWS_drive_read());                                           //Aguardo ele subir
    while(OWS_drive_read());											//Delay do tempo de leitura
    Delay_us(0);
    read_data = OWS_drive_read();                                       //Faz a leitura do estado do port

    return read_data;

}

/**********************************************************************
* Function:        void OW_write_byte (unsigned char write_data)
* PreCondition:    None
* Input:		   Send byte to 1-wire slave device
* Output:		   None
* Overview:		   This function used to transmit a complete byte to slave device.
*
***********************************************************************/

void OWS_write_byte(unsigned char write_data, unsigned char first){

	unsigned char loop;

	for(loop = 0; loop < 8; loop++){

		if(write_data & 1){
			if(first == HIGH)
				OWS_write_bit(FIRST_BIT);
			else
				OWS_write_bit(HIGH);
		}
		else
        	OWS_write_bit(LOW);
		write_data >>= 1;
	}

}

/**********************************************************************
* Function:        unsigned char OW_read_byte (void)
* PreCondition:    None
* Input:		   None
* Output:		   Return the read byte from slave device
* Overview:		   This function used to read a complete byte from the slave device.
*
***********************************************************************/

unsigned char OWS_read_byte(){

	unsigned char loop, result=0;
	GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_IN);

	for (loop = 0; loop < 8; loop++){

          result >>= 1; 				// shift the result to get it ready for the next bit to receive
          if(OWS_read_bit())
          result |= 0x80;				// if result is one, then set MS-bit
	}
	Delay_us(15);
	return result;

}


// send presence pulse
void OWS_present(){

	while(!OWS_drive_read());
	GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_OUT);
	Delay_us(5);
	OWS_drive_low();
	Delay_us(55);
	OWS_drive_high();

}

unsigned char OWS_wait_reset(){

	GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_IN);

	unsigned char a = OWS_drive_read();
	unsigned short int b;

	if(a == 0){
		while(a == 0){                               										//Enquanto estiver em n\EDvel baixo

			b = b + 10;                               										//Soma 10us na vari\E1vel b
			Delay_us(10);
			a = OWS_drive_read();							                            	//Verifica se o port ainda est\E1 em 0

		}
		if (b > 300)
			return HIGH;                      										        //Se b for maior que 300us ap\F3s sair do n\EDvel baixo... \E9 pq foi um pulso de reset e retorna 1
		else
			return LOW;                              										//Se for menor... n\E3o foi pulso de reset... retorna 0
	}
	else
		return LOW;
}

unsigned char* RFID_to_iButton(unsigned char *id){

	static unsigned char iButton_code[8];

	unsigned char count_RFID = 5;
	unsigned char count_OW;

	iButton_code[0] = 0x01;

	for(count_OW = 1; count_OW < 7; count_OW++){
		iButton_code[count_OW] = id[count_RFID];
		count_RFID--;
	}
	iButton_code[7] = crc8(iButton_code, 7);

	return iButton_code;
}

void send_iButton_code(unsigned char *iButton_code){

	unsigned char count_OW;

	OWS_write_byte(iButton_code[0], HIGH);
	for(count_OW = 1; count_OW < 8; count_OW++)
		OWS_write_byte(iButton_code[count_OW], LOW);

	GPIO_Config(oneWirePort, oneWirePin, GPIO_Mode_IN);

}

