/**
  ******************************************************************************
  * @file    gpio.c
  * @author  Mateus
  * @version V1.0
  * @date    07-April-2018
  * @brief   gpio function.
  ******************************************************************************
*/

#include "stm32f0xx_gpio.h"


void GPIO_Config(GPIO_TypeDef* GPIOx, uint32_t GPIO_Pin, uint8_t Mode){

	GPIO_InitTypeDef GPIO_Configure;

	if(GPIOx == GPIOA)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	else if(GPIOx == GPIOB)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	else if(GPIOx == GPIOC)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	else if(GPIOx == GPIOD)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
	else
		return;

	GPIO_Configure.GPIO_Pin = GPIO_Pin;
	GPIO_Configure.GPIO_Mode = Mode;
	GPIO_Configure.GPIO_OType = GPIO_OType_PP;
	GPIO_Configure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Configure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	GPIO_Init(GPIOx, &GPIO_Configure);

}
